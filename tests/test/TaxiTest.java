package test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.SequentialSearchST;
import model.vo.Servicio;
import model.vo.Taxi;

public class TaxiTest 
{
	private Taxi taxi;
	/**
     * Crea una instancia de la clase Compania. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
        taxi = new Taxi("2","CityTaxi");
    }
	
	@Test
	public void testServicio( )
    {
        assertTrue("El servicio no se cre� correctamente", taxi!=null);
    }
	
	@Test
	public void testAsignarHash()
	{
		SequentialSearchST<String, ArrayList<Servicio>> nuevoHash= new SequentialSearchST<String, ArrayList<Servicio>>();
		taxi.asignarHash(nuevoHash);
		assertTrue("El hash no se asign� correctamente", nuevoHash!=null);
	}
}
