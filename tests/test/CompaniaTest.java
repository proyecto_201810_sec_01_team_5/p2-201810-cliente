package test;

import static org.junit.Assert.*; 

import org.junit.Before;
import org.junit.Test;

import model.vo.Compania;
import model.vo.Taxi;
import model.vo.TaxiConServicios;

public class CompaniaTest 
{
	private Compania compania;
	/**
     * Crea una instancia de la clase Compania. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
        compania = new Compania( "CityTaxi" );
    }
    
	
	@Test
    public void testCompania()
    {
    	assertTrue("El nombre de la compa�ia no es correcto", compania.getNombreCompania().equals("CityTaxi"));
    	assertTrue("El ArrayList se carg� incorrectamente", compania.getTaxisInscritos().size()==0);
    }
    
	@Test
	public void agregarTaxiTaxi()
	{
		TaxiConServicios nuevoTaxi=new TaxiConServicios("2", "CityTaxi");
		compania.a�adirTaxi(nuevoTaxi);
		assertTrue("El nuevo taxi no se carg� correctamente", compania.getTaxisInscritos().size()!=0);
		
	}
}
