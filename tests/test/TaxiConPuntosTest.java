package test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.SequentialSearchST;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;

public class TaxiConPuntosTest 
{
	private TaxiConPuntos taxi;
	/**
     * Crea una instancia de la clase Compania. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
        taxi = new TaxiConPuntos("2","CityTaxi");
    }
	
	@Test
	public void testServicio( )
    {
        assertTrue("El servicio no se cre� correctamente", taxi!=null);
        assertTrue("El ArrayList no se carg� correctamente", taxi.getServicios()!=null);
    }
	
	@Test
	public void testAsignarHash()
	{
		taxi.cambiarPuntos(2);
		assertTrue("El hash no se asign� correctamente", taxi.getPuntos()==2);
	}
}
