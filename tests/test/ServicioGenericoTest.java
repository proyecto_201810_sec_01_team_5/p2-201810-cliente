package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import model.vo.ServicioGenerico;

public class ServicioGenericoTest 
{

	private ServicioGenerico servicio;
	/**
     * Crea una instancia de la clase Compania. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
        servicio = new ServicioGenerico();
    }
	
	@Test
	public void testServicio( )
    {
        assertTrue("El servicio no se cre� correctamente", servicio!=null);
    }
}
