package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.Lista.Iterador;

public class Lista<T extends Comparable<T>> implements IList<T> {

	private Node<T> head=null;
	private Node<T> tail=null;
	private Node<T> actual=null;
	private int size=0;
	public Lista()
	{
		//TODO completar
	}
	@Override
	public void add(T elem) {
		// TODO Auto-generated method stub
		Node<T> newest=new Node(elem,null);
		if(size==0)
		{
			head=newest;
		}
		else
		{
			tail.setNext(newest);
			newest.setPrevious(tail);
		}
		tail=newest;
		size++;
	}

	@Override
	public T get(T elem) 
	{
		// TODO Auto-generated method stub
		T res=null;
		actual=(Node<T>) iterator().next();
		while(actual!=null)
		{
			if(actual.getElement().compareTo(elem)==0)
			{
				res=actual.getElement();
				actual=null;
			}
			else
			{
				actual=actual.getNext();
			}
		}
		return res;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iterador = new Iterador<T>(head);
		return iterador;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	public class Iterador <K> implements Iterator <K>  {

		Node<K> actual;

		public Iterador (Node<K> PprimerElemento){
			actual = PprimerElemento; 
		}

		@Override
		public boolean hasNext() {

			if(size() == 0){
				return false;
			}

			if(actual == null)
				return true;
			return actual.getNext() != null;
		}

		@Override
		public K next() {
			if(actual == null)
				actual = (Node<K>) head;
			else{
				actual = actual.getNext();
			}
			return actual.getElement();
		}

	}


}
