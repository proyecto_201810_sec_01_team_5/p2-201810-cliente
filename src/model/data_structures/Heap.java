package model.data_structures;

import java.util.ArrayList;

public class Heap<T extends Comparable<T>> {
	private static final int CAPACITY = 2;

	private int size;
	private T[] heap;

	public Heap()
	{
		size = 0;
		heap = (T[]) new Comparable[CAPACITY];
	}
	
	public ArrayList darHeap()
	{
		ArrayList<T> res=new ArrayList<T>();
		for(int i=0; i <heap.length;i++)
		{
			if(heap[i]!=null)
			{
				res.add(heap[i]);
				//System.out.println(heap[i]);
			}
		}
		return res;
	}
	public Heap(T[] array)
	{
		size = array.length;
		heap  = (T[]) new Comparable[array.length+1];

		System.arraycopy(array, 0, heap, 1, array.length);

		buildHeap();
	}

	private void buildHeap()
	{
		for(int k = size / 2; k>0; k--)
		{
			percolatingDown(k);
		}
	}

	private void percolatingDown(int k)
	{
		T tmp = heap[k];
		int child;

		for(; 2*k <= size; k = child)
		{
			child = 2*k;

			if(child != size &&
					heap[child].compareTo(heap[child + 1]) > 0) child++;

			if(tmp.compareTo(heap[child]) > 0)  heap[k] = heap[child];
			else
				break;
		}
		heap[k] = tmp;
	}

	public void heapSort(T[] array)
	{
		size = array.length;
		heap = (T[]) new Comparable[size+1];
		System.arraycopy(array, 0, heap, 1, size);
		buildHeap();

		for (int i = size; i > 0; i--)
		{
			T tmp = heap[i]; //move top item to the end of the heap array
			heap[i] = heap[1];
			heap[1] = tmp;
			size--;
			percolatingDown(1);
		}
		for(int k = 0; k < heap.length-1; k++)
			array[k] = heap[heap.length - 1 - k];
	}

	public T deleteMin()
	{

		T min = heap[1];
		heap[1] = heap[size--];
		percolatingDown(1);
		return min;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void insert(T x)
	{
		if(size == heap.length - 1) doubleSize();

		//Insert a new item to the end of the array
		int pos = ++size;

		//Percolate up
		for(; pos > 1 && x.compareTo(heap[pos/2]) < 0; pos = pos/2 )
			heap[pos] = heap[pos/2];

		heap[pos] = x;
	}

	public int size()
	{
		return size;
	}

	public T peek() {
		return heap[1];
	}

	private void doubleSize()
	{
		T [] old = heap;
		heap = (T []) new Comparable[heap.length * 2];
		System.arraycopy(old, 1, heap, 1, size);
	}
}
