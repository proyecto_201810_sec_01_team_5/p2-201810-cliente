package model.data_structures;



public class Node<E> 
{
		private E element;
		private Node<E> next;
		private Node<E> previous;
		public Node(E e, Node<E>n)
		{
			element=e;
			next =n;
		}
		public void setElement(E nuevo)
		{
			this.element = nuevo;
		}
		public E getElement()
		{
			return element;
		}
		public Node<E> getNext()
		{
			return next;
		}
		public Node<E> getPrevious()
		{
			return next;
		}
		public void setNext(Node<E> n)
		{
			next=n;
		}
		public void setPrevious(Node<E> n)
		{
			previous=n;
		}
	
}


