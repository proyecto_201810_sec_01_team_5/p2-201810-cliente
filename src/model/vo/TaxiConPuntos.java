package model.vo;

import java.util.ArrayList;

import model.data_structures.IList;
import model.data_structures.SequentialSearchST;

public class TaxiConPuntos implements Comparable<TaxiConPuntos>
{

	private String taxi_id;
    private String company;
    private ArrayList<Servicio> servicios=new ArrayList<Servicio>();
    private double puntos;
    
	public TaxiConPuntos(String taxiId, String compania) {
		taxi_id=taxiId;
		company=compania;
		servicios=new ArrayList<Servicio>();
		puntos=getPuntos();
		// TODO Auto-generated constructor stub
	}
	public void crearServicios()
	{
		servicios=new ArrayList<Servicio>();
		puntos=getPuntos();
	}
	public ArrayList<Servicio> getServicios()
	{
		return servicios;
	}
	public void agregarServicio(Servicio s)
	{
		if(servicios!=null && s!=null)
		{
			servicios.add(s);
		}	
	}
	public void cambiarPuntos(double nuevos)
	{
		puntos=nuevos;
	}
	/**
     * @return puntos - puntos de un Taxi
     */
	public double getPuntos()
	{
		double dineroRecibido=1;
		double millasRecorridas=1;
		double carreras=0;
		
		if(servicios!=null && puntos!=-2)
		{
			for(int i=0; i<servicios.size();i++)
			{
				if(servicios.get(i)!=null)
				{
					dineroRecibido+=servicios.get(i).getTripTotal();
					millasRecorridas+=servicios.get(i).getTripMiles();
					if(servicios.get(i).getTripMiles()>0 && servicios.get(i).getTripTotal()>0)
					{
						carreras++;
					}
				}	
			}
		}
		else if(puntos==-2)
		{
			puntos=-2;
		}
		
		double puntos=(dineroRecibido/millasRecorridas)*carreras;
		return puntos;
	}

	/**
	 * @return the taxiId
	 */
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @param taxiId the taxiId to set
	 */
	public void setTaxiId(String taxiId) {
		this.taxi_id = taxiId;
	}

	/**
	 * @return the compania
	 */
	public String getCompania() {
		return company;
	}

	/**
	 * @param compania the compania to set
	 */
	public void setCompania(String compania) {
		this.company = compania;
	}
	 
	    public int compareTo(TaxiConPuntos o)
	    {
	        // TODO Auto-generated method stub
	    	int res=1;
	    	if(getPuntos()<(o.getPuntos()))
	    	{
	    		res=-1;
	    	}
	    	else if(getPuntos()==(o.getPuntos()))
	    	{
	    		res=0;
	    	}
	        return res;
	    }
}
