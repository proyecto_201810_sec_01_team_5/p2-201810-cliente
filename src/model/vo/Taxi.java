package model.vo;

import java.util.ArrayList;

import model.data_structures.SequentialSearchST;

public class Taxi implements Comparable<Taxi> {

	private String taxi_id;
	private String company;
	private SequentialSearchST<String, ArrayList<Servicio>> hashTaxis=null;
	
	public Taxi(String taxiId, String compania)
	{
		taxi_id=taxiId;
		company=compania;
		hashTaxis=null;
	}
	
	public void asignarHash(SequentialSearchST<String, ArrayList<Servicio>> nuevoHash)
	{
		hashTaxis=nuevoHash;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() 
	{
		return company;
	}
	
    @Override
    public int compareTo(Taxi o)
    {
        // TODO Auto-generated method stub
        return 0;
    }
}
