package model.vo;

import java.awt.List;
import java.util.ArrayList;

import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.SequentialSearchST;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

	private int numeroServicios=0;
    private String taxiId;
    private String compania;
    private IList<Servicio> servicios;
    private SequentialSearchST<String, ArrayList<Servicio>> hashTaxis=null;
	
    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        hashTaxis=null;
        servicios = new Lista<Servicio>(); // inicializar la lista de servicios 
    }

    public void asignarHash(SequentialSearchST<String, ArrayList<Servicio>> nuevoHash)
	{
		hashTaxis=nuevoHash;
		for(int i=1; i<77; i++)
		{
			if(hashTaxis.get(Integer.toString(i))!=null)
			{
				numeroServicios+=hashTaxis.get(Integer.toString(i)).size();
				//System.out.println("Tama�o: " + numeroServicios);
			}
		}
	}
    
    public SequentialSearchST<String, ArrayList<Servicio>> getHashTable()
	{
		return hashTaxis;
	}
    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

   public IList<Servicio> getServicios()
   {
	   return servicios;
   }
    
   public int numeroServicios()
   {
	  return numeroServicios;
   }

   public void agregarServicio(Servicio servicio){
        servicios.add(servicio);
    }

    @Override
    public int compareTo(TaxiConServicios o) {
        return taxiId.compareTo( o.getTaxiId());
    }
    
    
    public void print()
    {
    	
    	System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
    	for(Servicio s : servicios){
            System.out.println("\t"+s.getStartTime());
        }
        System.out.println("___________________________________");;
    }
}
