package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	private String company;
	private String dropoff_community_area;
	private String pickup_centroid_latitude;
	private String pickup_centroid_longitude;
	private String pickup_community_area;
	private String taxi_id;
	private String trip_id;
	private String trip_miles;
	private String trip_seconds;
	private String trip_start_timestamp;
	private String trip_total;
	
	 public String getTripId() {
         // TODO Auto-generated method stub
         return trip_id;
     }

     /**
      * @return id - Taxi_id
      */
     public String getTaxiId() {
         // TODO Auto-generated method stub
         return taxi_id;
     }

     /**
      * @return time - Time of the trip in seconds.
      */
     public int getTripSeconds() {
         // TODO Auto-generated method stub
         return Integer.parseInt(trip_seconds);
     }

     /**
      * @return miles - Distance of the trip in miles.
      */
     public double getTripMiles() {
         // TODO Auto-generated method stub
         return Double.parseDouble(trip_miles);
     }

     /**
      * @return total - Total cost of the trip
      */
     public double getTripTotal() {
         // TODO Auto-generated method stub
         return Double.parseDouble(trip_total);
     }

     public String getStartTime(){
         //TODO Auto-generated method stub
         return trip_start_timestamp;
     }

     public int getPickupZone(){
         //TODO Auto-generated method stub
         return Integer.parseInt(pickup_community_area);
     }

     public int getDropOffZone(){
         //TODO Auto-generated method stub
         return Integer.parseInt(dropoff_community_area);
     }


     public double getPickupLatitud()
     {
         //TODO Auto-generated method stub
    	 if(pickup_centroid_latitude!=null)
    	 {
    		 return Double.parseDouble(pickup_centroid_latitude);
    	 }
    	 else
    	 {
    		 return 0.0;
    	 }
     }

     public double getPickupLongitud(){
         //TODO Auto-generated method stub
    	 if(pickup_centroid_longitude!=null)
    	 {
    		 return Double.parseDouble(pickup_centroid_longitude);
    	 }
    	 else
    	 {
    		 return 0.0;
    	 }
         
     }

     @Override
     public int compareTo(Servicio arg0) {
         // TODO Auto-generated method stub
         return this.getTripId().compareTo( arg0.getTripId() );
     }

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	
	
	/**
	 * @return the dropoff_community_area
	 */
	public String getDropoff_community_area() {
		return dropoff_community_area;
	}
	/**
	 * @param dropoff_community_area the dropoff_community_area to set
	 */
	public void setDropoff_community_area(String dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}
	
	
	
	
	
	/**
	 * @return the pickup_centroid_latitude
	 */
	public String getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}
	/**
	 * @param pickup_centroid_latitude the pickup_centroid_latitude to set
	 */
	public void setPickup_centroid_latitude(String pickup_centroid_latitude) {
		this.pickup_centroid_latitude = pickup_centroid_latitude;
	}
	
	/**
	 * @return the pickup_centroid_longitude
	 */
	public String getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}
	/**
	 * @param pickup_centroid_longitude the pickup_centroid_longitude to set
	 */
	public void setPickup_centroid_longitude(String pickup_centroid_longitude) {
		this.pickup_centroid_longitude = pickup_centroid_longitude;
	}
	/**
	 * @return the pickup_community_area
	 */
	public String getPickup_community_area() {
		return pickup_community_area;
	}
	/**
	 * @param pickup_community_area the pickup_community_area to set
	 */
	public void setPickup_community_area(String pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}
	/**
	 * @return the taxi_id
	 */
	public String getTaxi_id() {
		return taxi_id;
	}
	/**
	 * @param taxi_id the taxi_id to set
	 */
	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}
	
	
	/**
	 * @return the trip_id
	 */
	public String getTrip_id() {
		return trip_id;
	}
	/**
	 * @param trip_id the trip_id to set
	 */
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	/**
	 * @return the trip_miles
	 */
	public String getTrip_miles() {
		return trip_miles;
	}
	/**
	 * @param trip_miles the trip_miles to set
	 */
	public void setTrip_miles(String trip_miles) {
		this.trip_miles = trip_miles;
	}
	/**
	 * @return the trip_seconds
	 */
	public String getTrip_seconds() {
		return trip_seconds;
	}
	/**
	 * @param trip_seconds the trip_seconds to set
	 */
	public void setTrip_seconds(String trip_seconds) {
		this.trip_seconds = trip_seconds;
	}
	/**
	 * @return the trip_start_timestamp
	 */
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}
	/**
	 * @param trip_start_timestamp the trip_start_timestamp to set
	 */
	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}
	/**
	 * @return the trip_total
	 */
	public String getTrip_total() {
		return trip_total;
	}
	/**
	 * @param trip_total the trip_total to set
	 */
	public void setTrip_total(String trip_total) {
		this.trip_total = trip_total;
	}
}
