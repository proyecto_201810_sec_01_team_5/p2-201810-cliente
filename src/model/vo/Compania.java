package model.vo;

import java.util.ArrayList;

import model.data_structures.SequentialSearchST;

public class Compania implements Comparable<Compania>
 {
	
	private String company;
	private ArrayList<TaxiConServicios> taxisInscritos=null;
	
	
	public Compania(String nombre)
	{
		company=nombre;
		taxisInscritos=new ArrayList<TaxiConServicios>();
		
	}
	
	public void añadirTaxi(TaxiConServicios nuevo)
	{
		taxisInscritos.add(nuevo);
	}
	
	public ArrayList getTaxisInscritos()
	{
		return taxisInscritos;
	}
	
	@Override
	public int compareTo(Compania o) {
		// 
		return 0;
	}


	/**
	 * @return the nombreCompania
	 */
	public String getNombreCompania() {
		return company;
	}


	/**
	 * @param nombreCompania the nombreCompania to set
	 */
	public void setNombreCompania(String nombreCompania) {
		this.company = nombreCompania;
	}
	
	
	

}
