package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.smartcardio.ResponseAPDU;
import javax.xml.ws.RespectBindingFeature;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import API.ITaxiTripsManager;
import model.data_structures.Heap;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.RedBlack;
import model.data_structures.SequentialSearchST;
import model.vo.Compania;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.ServicioGenerico;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	public static final String DIRECCION_MINI_JSON = "./data/taxi-trips-wrvz-psew-subset-mini.json";
	
	//Programa
	ServicioGenerico[] backupDatos=null;
	ServicioGenerico[] servicios=null;
	Servicio[] backupServicios2=null;
	Servicio[] servicios2=null;
	TaxiConPuntos[] taxisPuntos=null;
	
	//Punto A1
	private RedBlack<String, Compania> arbolA1=null;
	ArrayList<Compania> companiasA1=null;
	
	//PuntoA2
	SequentialSearchST<String, ArrayList<Servicio>> hashA2=null;
	
	//Punto C1
	Heap<TaxiConPuntos> heapC1=new Heap<TaxiConPuntos>();
	ArrayList<TaxiConPuntos> arregloTaxiPuntos=null;
	
	//Punto C2
	SequentialSearchST<String, ArrayList<Servicio>> hashC2=null;
	double latitudCentral=0;
	double longitudCentral=0;
	
	//Punto C3
	private RedBlack<String, Servicio> arbolC3=null;
	
	@Override
	public boolean cargarSistema(String serviceFile) 
	{
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);
		
		BufferedReader reader=null;
		BufferedReader reader2=null;
		BufferedReader reader3=null;
		try
		{
			//Carga de datos y prueba
			reader=new BufferedReader(new FileReader(serviceFile));
			reader2=new BufferedReader(new FileReader(serviceFile));
			reader3=new BufferedReader(new FileReader(serviceFile));
			
			Gson gson1=new GsonBuilder().create();
			servicios=gson1.fromJson(reader, ServicioGenerico[].class);
			servicios2=gson1.fromJson(reader3, Servicio[].class);
			backupDatos=servicios;
			backupServicios2=servicios2;
			taxisPuntos=gson1.fromJson(reader2, TaxiConPuntos[].class);
			System.out.println("object mode: " + servicios[0].getCompany());
			System.out.println("Prueba: " + taxisPuntos[0].getCompania());
			
			//Punto A1
			//Creaci�n �rbol balanceado y hash
			arbolA1=new RedBlack<String, Compania>();
			companiasA1=new ArrayList<Compania>();
			Compania taxisSinCompania=new Compania("Independent Owner");
			companiasA1.add(taxisSinCompania);
			for(int i=0; i<servicios.length;i++)
			{
				ServicioGenerico actual=servicios[i];
				if(actual!=null && actual.getCompany()!=null)
				{
					if(!(actual.getCompany().equals("")||actual.getCompany().equals("ELIMINADA")))
					{
						String nombreActual=actual.getCompany();
						Compania nuevaCompania=new Compania(nombreActual);
						companiasA1.add(nuevaCompania);
						for(int j=0; j<servicios.length;j++)
						{
							ServicioGenerico actual2=servicios[j];
							if((actual2!=null && actual2.getCompany()!=null) && (actual2.getCompany().equals(nuevaCompania.getNombreCompania())))
							{
								TaxiConServicios taxiNuevo=new TaxiConServicios(actual2.getTaxi_id(), actual2.getCompany());
								nuevaCompania.a�adirTaxi(taxiNuevo);
								actual2.setCompany("ELIMINADA");
							}
						}
					}
				}
				else if((actual!=null && actual.getCompany()!=null && actual.getCompany().equals("")) || (actual!=null && actual.getCompany()==null))
				{
					TaxiConServicios taxiNuevo=new TaxiConServicios(actual.getTaxi_id(), actual.getCompany());
					taxisSinCompania.a�adirTaxi(taxiNuevo);
				}
			}	
			for(int i=0; i<companiasA1.size();i++)
			{
				System.out.println(companiasA1.get(i).getNombreCompania());
			}
			restaurarDatos();
			for(int i=0; i<companiasA1.size();i++)
			{
				Compania actual=companiasA1.get(i);
				String codigo=actual.getNombreCompania();
				arbolA1.put(codigo, actual);
				for(int j=0; j<actual.getTaxisInscritos().size(); j++)
				{
					TaxiConServicios taxiActual=(TaxiConServicios) actual.getTaxisInscritos().get(j);
					SequentialSearchST<String, ArrayList<Servicio>> hashTemporal=new SequentialSearchST<String, ArrayList<Servicio>>();
					for(int k=zonaMinima(); k<zonaMaxima();k++)
					{
						ArrayList<Servicio> temporal=new ArrayList<Servicio>();
						for(int n=0; n<servicios2.length;n++)
						{
							Servicio servActual=servicios2[n];
							if(servActual.getTaxi_id().equals(taxiActual.getTaxiId()) && servActual!= null && servActual.getPickup_community_area()!=null)
							{
								if(servActual.getPickup_community_area().equals(Integer.toString(k)))
								{	
									if(temporal!=null)
									{
										boolean repetido=false;
										for(int m=0; m<temporal.size();m++)
										{
											if(temporal.get(m).equals(servActual))
											{
												repetido=true;
											}
										}
										if(repetido==false)
										{
											temporal.add(servActual);
										}		
									}				
								}
							}
						}
						if(temporal.size()!=0)
						{
							hashTemporal.put(Integer.toString(k), temporal);
						}				
					}
					if(hashTemporal.size()>0)
					{
						taxiActual.asignarHash(hashTemporal);
					}		
				}	
			}	
			restaurarDatos();
			
			//Punto A2
			//Creaci�n de tabla de Hash
			hashA2=new SequentialSearchST<String, ArrayList<Servicio>>();
			int tiempoMax=tiempoMaximo();
			int tiempoPiso=1;
			int tiempoTecho=60;
			while(tiempoTecho<=tiempoMax)
			{
				ArrayList<Servicio> arrayTemporal=new ArrayList<Servicio>();
				for(int i=0; i <servicios2.length;i++)
				{
					Servicio actual=servicios2[i];
					if(actual.getTripSeconds()>=tiempoPiso && actual.getTripSeconds()<=tiempoTecho)
					{
						arrayTemporal.add(actual);
					}
				}
				if(arrayTemporal.size()>0)
				{
					hashA2.put(tiempoPiso+"-"+tiempoTecho, arrayTemporal);
				}
				tiempoPiso+=60;
				tiempoTecho+=60;
			}	
			restaurarDatos();
			
			
			//Punto C1
			//Creaci�n del Heap
			for(int h=0; h<taxisPuntos.length;h++)
			{
				taxisPuntos[h].crearServicios();
			}
			for(int i=0; i<taxisPuntos.length;i++)
			{
				TaxiConPuntos actual=taxisPuntos[i];
				if(actual!=null)
				{
					for(int j=0; j< servicios2.length;j++)
					{
						Servicio servicioActual=servicios2[j];
						if(servicioActual!=null)
						{
							if(servicioActual.getTaxi_id()!=null && servicioActual.getTaxi_id().equals(actual.getTaxiId()))
							{
								actual.agregarServicio(servicioActual);
							}
						}
					}
					heapC1.insert(actual);
				}
			}
			restaurarDatos();
			
			
			//Punto C2
			//Creaci�n Tabla de Hash
			hashC2=new SequentialSearchST<String, ArrayList<Servicio>>();
			coordenadasCentrales();
			BigDecimal distanciaMaxima=BigDecimal.valueOf(distanciaMaximaC2());
			System.out.println("Distancia maxima: " +distanciaMaxima);
			BigDecimal distanciaPiso=BigDecimal.valueOf(0.1);
			BigDecimal distanciaTecho=BigDecimal.valueOf(0.2);
			
			while(distanciaTecho.compareTo(distanciaMaxima)<=0)
			{
				ArrayList<Servicio> arrayTemporal2=new ArrayList<Servicio>();
				for(int i=0; i <servicios2.length;i++)
				{
					Servicio actual=servicios2[i];
					if(actual!=null)
					{
						if(BigDecimal.valueOf(distanciaAlCentro(actual)).compareTo(distanciaPiso) >=0 && BigDecimal.valueOf(distanciaAlCentro(actual)).compareTo(distanciaTecho) <0)
						{
							arrayTemporal2.add(actual);
						}
					}	
				}
				if(arrayTemporal2.size()>0)
				{
					hashC2.put(distanciaPiso+"-"+distanciaTecho, arrayTemporal2);
				}
				distanciaPiso=distanciaPiso.add(BigDecimal.valueOf(0.1));
				distanciaTecho=distanciaTecho.add(BigDecimal.valueOf(0.1));
			}	
			restaurarDatos();
			
			//Punto C3
			arbolC3=new RedBlack<String, Servicio>();
			for(int i=0; i< servicios2.length;i++)
			{
				Servicio actual=servicios2[i];
				if(actual!=null)
				{
					arbolC3.put(actual.getTrip_id(), actual);
				}
			}
			restaurarDatos();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("No se encuentra el archivo");
		}
		finally
		{
			System.out.println("Listo!");
		}
		return false;
	}

	
	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) 
	{
		// TODO Auto-generated method stub
		Lista<TaxiConServicios> listaRes= new Lista<TaxiConServicios>();
		int maximoServicios=0;
		ArrayList<TaxiConServicios> auxiliar=new ArrayList<TaxiConServicios>();
		Compania buscada = arbolA1.get(compania);
		ArrayList<TaxiConServicios> taxisBuscada=buscada.getTaxisInscritos();
		ArrayList<TaxiConServicios> pasados=new ArrayList<TaxiConServicios>();
		
		if(buscada!=null)
		{
			for(int i=0; i < taxisBuscada.size(); i++)
			{
				TaxiConServicios actual=(TaxiConServicios) taxisBuscada.get(i);
				SequentialSearchST<String, ArrayList<Servicio>> tablaActual = actual.getHashTable();
				if(tablaActual!=null)
				{
					ArrayList<Servicio> listaServicios=tablaActual.get(Integer.toString(zonaInicio));
					if(listaServicios!=null && listaServicios.size()>=maximoServicios)
					{
						auxiliar.add(actual);
						maximoServicios=listaServicios.size();
					}
				}
			}
			for(int j=0;j<auxiliar.size();j++)
			{
				TaxiConServicios actual=auxiliar.get(j);
				if(actual!=null && actual.getHashTable().get(Integer.toString(zonaInicio)).size()==maximoServicios)
				{
					boolean aviso=false;
					for(int m=0; m<pasados.size();m++)
					{
						if(pasados.get(m).equals(actual))
						{
							aviso=true;
						}
					}
					if(aviso==false)
					{
						listaRes.add(actual);
					}
				}
			}
		}
		return listaRes;
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) 
	{
		// TODO Auto-generated method stub
		Lista<Servicio> listaRes= new Lista<Servicio>();
		ArrayList<Servicio> auxiliar=new ArrayList<Servicio>();
		//Determinaci�n de rango
		int inicial=0;
		int ifinal=60;
		String rango="";
		boolean indicador=false;
		
		while(indicador==false)
		{
			if(duracion>=inicial && duracion<=ifinal)
			{
				indicador=true;
				rango=inicial+1+"-"+ifinal;
				auxiliar=hashA2.get(rango);
			}
			else
			{
				inicial+=60;
				ifinal+=60;
			}
		}		
		if(auxiliar!=null)
		{
			for(int j=0; j<auxiliar.size();j++)
			{
				if(auxiliar.get(j)!=null)
				{
					listaRes.add(auxiliar.get(j));
				}
			}
		}	
		return listaRes;
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() 
	{
		// TODO Auto-generated method stub
		ArrayList<TaxiConPuntos> aux=heapC1.darHeap();
		TaxiConPuntos[] res= new TaxiConPuntos[aux.size()];
		ArrayList<TaxiConPuntos> aux2=new ArrayList<TaxiConPuntos>();;
		aux2=ordenarArregloC1(aux);
		for(int i=0; i<aux2.size();i++)
		{
			if(aux2.get(i)!=null)
			{
				res[i]=aux2.get(i);
			}
		}	
		return res;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C)
	{
		ArrayList<Servicio> auxiliar=new ArrayList<Servicio>();
		Lista<Servicio> res=new Lista<Servicio>();
		String rango="";
		
		double piso=Math.floor(millasReq2C*10d)/10d;
		double techo=Math.floor((piso+0.1)*10d)/10d;
		rango=piso+"-"+techo;
		auxiliar=hashC2.get(rango);
		
		if(auxiliar!=null)
		{
			for(int i=0; i<auxiliar.size();i++)
			{
				Servicio actual=auxiliar.get(i);
				if(actual!=null)
				{
					String taxiId=actual.getTaxi_id();
					if(taxiId!=null)
					{
						if(taxiId.equals(taxiIDReq2C))
						{
							res.add(actual);
						}
					}
				}
			}
		}
		if(res.size()==0)
		{
			System.out.println("El taxi no tiene servicios a esa distancia");
		}
		return res;
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) 
	{
		// TODO Auto-generated method stub
		Lista<Servicio> respuesta=new Lista<Servicio>();
		for(int i=0; i< servicios2.length;i++)
		{
			Servicio actual = servicios2[i];
			if(actual!=null)
			{
				Servicio a�adido=arbolC3.get(actual.getTrip_id());
				if(a�adido.getTrip_start_timestamp().equals(fecha+"T"+hora))
				{
					if(!a�adido.getPickup_community_area().equals(a�adido.getDropoff_community_area()))
					{
						respuesta.add(a�adido);
					}
				}
			}
		}
		return respuesta;
	}


	public void restaurarDatos()
	{
		servicios=backupDatos;
		servicios2=backupServicios2;
	}
	
	public int zonaMinima()
	{
		int min=9999999;
		int res=0;
		for(int j=0; j<servicios.length;j++)
		{
			ServicioGenerico actual=servicios[j];
			if(actual!=null && actual.getPickup_community_area()!=null)
			{
				if(Integer.parseInt(actual.getPickup_community_area())<=min)
				{
					min=Integer.parseInt(actual.getPickup_community_area());
				}
			}
		}
		res=min;
		//System.out.println("La zona minima es " + res);
		return res;
	}
	
	public int zonaMaxima()
	{
		int max=-1;
		int res=0;
		for(int j=0; j<servicios.length;j++)
		{
			ServicioGenerico actual=servicios[j];
			if(actual!=null && actual.getPickup_community_area()!=null)
			{
				if(Integer.parseInt(actual.getPickup_community_area())>=max)
				{
					max=Integer.parseInt(actual.getPickup_community_area());
				}
			}
		}
		res=max;
		return res;
	}

	public int tiempoMaximo()
	{
		int maximaDuracion=0;
		for(int i=0; i<servicios2.length;i++)
		{
			Servicio actual=servicios2[i];
			if(actual!=null)
			{
				int tiempoActual=actual.getTripSeconds();
				if(tiempoActual>maximaDuracion)
				{
					maximaDuracion=tiempoActual;
				}
			}	
		}
		return maximaDuracion;
	}
	
	public void coordenadasCentrales()
	{
		double sumaLatitud=0;
		double sumaLongitud=0;
		for(int i=0; i<servicios2.length;i++)
		{
			sumaLatitud+=servicios2[i].getPickupLatitud();
			sumaLongitud+=servicios2[i].getPickupLongitud();
		}
		latitudCentral=sumaLatitud/servicios2.length;
		longitudCentral=sumaLongitud/servicios2.length;
	}
	public double distanciaAlCentro(Servicio servicio)
	{
		double respuesta=0;
		final int R=6371*1000;
		double lat1=servicio.getPickupLatitud();
		double lon1=servicio.getPickupLongitud();
		double lat2=latitudCentral;
		double lon2=longitudCentral;
		
		 double latDistance = Math.toRadians(lat2-lat1);
		 double lonDistance = Math.toRadians(lon2-lon1);
		 double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		 double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		 respuesta = R * c * 0.00000621371;
		return respuesta;
	}
	public double distanciaMaximaC2()
	{
		double distanciaMaxima=0;
		for(int i=0; i<servicios2.length;i++)
		{
			Servicio actual=servicios2[i];
			if(actual!=null)
			{
				double distanciaActual=distanciaAlCentro(actual);
				if(distanciaActual>distanciaMaxima)
				{
					distanciaMaxima=distanciaActual;
				}
			}	
		}
		return distanciaMaxima;
	}
	public ArrayList<TaxiConPuntos> ordenarArregloC1(ArrayList<TaxiConPuntos> array)
	{
		ArrayList<TaxiConPuntos> respuesta=new ArrayList<TaxiConPuntos>();
		
		for(int i=0; i<array.size();i++)
		{
			double max=-1;
			int posCambiar=-1;
			for(int j=0; j< array.size();j++)
			{
				if(array.get(j)!= null && array.get(j).getPuntos()>max)
				{
					max=array.get(j).getPuntos();
					posCambiar=j;
				}
			}
			respuesta.add(array.get(posCambiar));
			array.get(posCambiar).cambiarPuntos(-2);
		}
		return respuesta;
	}
	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI,
			String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return null;
	}

}
